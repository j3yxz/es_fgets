#ifndef __ES_FGETS_H__
	#define __ES_FGETS_H__



	char *es_fgets(void);
	
	/* the usage of the function is:
	char* var=es_gets();

	the function will ask for input and continue to read anything until \n (enter) and put it
	in memory (calloc). Then it will return a pointer to the start of the memory where the string
	is. It will never stop reading from stdin until \n or \0 (tested up to 4GB of caracters, thanks to a friend of mine).

	Do NOT use it with scanf() s_scanf() or similar, this is implemented using fgets() from <stdio.h>
	so as you may already know fgets() and scanf() will mess up each other if used on same file.
	*/
#endif
