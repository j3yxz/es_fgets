#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define __ES_FGETS_H__

char *es_fgets(void){                                                   //si definisce la funzione che permette l'inserimento delle parole

    unsigned int dimStringhe=30;                                        //dimensione iniziale delle stringhe
    char *tmp=(char*)calloc(sizeof(char), dimStringhe);                 //si usa calloc per non avere memoria sporca, tmp è un puntatore a vettore
                                                                        //che verrà utilizzato per copiare un po' alla volta-
    char *dest=(char*)calloc(sizeof(char), dimStringhe+4);              //-la stringa dell'utente che andrà in dest che ha dimensione maggiore rispetto a tmp



        fgets(tmp, dimStringhe, stdin);                                 //l'utente inserisce la parola
        strcpy(dest, tmp);                                              //si prendono dimStringhe caratteri e si copiano in dest

        /*
            La fgets aggiunge automaticamente '\0' alla fine dei caratteri che legge da
            stdin che, quindi, sono al più dimStringhe-1 che vengono copiati in dest, quindi leggendo
            il carattere di posizione dimStringhe-2 se è diverso da '\0' significa che la stringa
            inserita dall'utente è più lunga (o è uguale a dimStringhe-2 , si controlla che sia !='\n'),
            in questo modo il ciclo perdura finchè lo stdin non si svuota completamente
        */

        while((*(tmp+dimStringhe-2) != '\0') && (*(tmp+dimStringhe-2) != '\n')){
            dimStringhe*=2;                                             //si raddoppia dimStringhe per gestire stringhe molto lunghe
            tmp=(char *)realloc(tmp, dimStringhe);                      //tmp viene ingrandita raddoppiando la dimensione
            if(tmp==NULL){                                              //controllo realloc()
                printf("Errore: memoria insufficiente\n");
                return NULL;
            }

            memset(tmp, 0, dimStringhe);                                //si pulisce tmp

            dest=(char *)realloc(dest, 2*dimStringhe);                  //si raddoppia la dimensione di dest
            if(dest==NULL){                                             //controllo realloc()
                printf("Errore: memoria insufficiente\n");
                return NULL;
            }

            fgets(tmp, dimStringhe, stdin);                             //fgets legge da stdin ciò che è rimasto in stdin,-
                                                                        //-non viene chiesto all'utente nuovo input perché lo-
                                                                        //-stdin è ancora in attesa e con il controllo !='\n'
                                                                        //si evita il rischio di chiedere l'input inutilmente

            strcat(dest, tmp);                                          //si copia il contenuto del nuovo tmp in coda a dest

        }
/*                                                                      se non si vogliono gli spazi
        int j=0;
        while(!isspace(dest[j])){                                       //si controlla che l'utente non inserisca più di una parola
            j++;
        
        
            if(dest[j]!='\n'){                                              
                printf("\nErrore: inserire una sola parola\n");
                return NULL;
                
            }

        }
*/
        if(dest[0]=='\n'){                                              //si controlla che l'utente non prema invio senza inserire nulla
            printf("\nErrore: inserire una parola\n");
            return NULL;
        }
        dest[strlen(dest)-1]='\0';										//metto il carattere di terminazione al posto di \n che mi troverei(opzionale ma non troppo)
        return dest;                                                    //la funzione restituisce il puntatore alla stringa completa
}